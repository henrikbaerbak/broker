/*
 * Copyright (C) 2018-2025. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package gamelobby.server;

import com.google.gson.Gson;
import io.javalin.Javalin;
import io.javalin.http.Context;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import kong.unirest.core.JsonNode;

import java.io.IOException;
import java.util.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/** A DEMONSTRATION ONLY game lobby server using REST
 * to make state changes in a game system using the
 * HATEOAS paradigm.
 *
 * NO ERROR CHECKING at all is implemented. Only
 * HAPPY PATH is supported, that is, no handling of
 * invalid moves, retrieving non-existing resources, etc.
 *
 * ONLY ONE FutureGame resource and ONE Game resource
 * is handled. No real domain code is implemented,
 * all are Fake-It code within the server.
 *
 * If you want to follow the interaction between
 * client and server, enable the println statement
 * in the debugOutput method and run the test case.
 */

public class GameLobbyRestServer {
  private final Gson gson;
  private final Javalin javalin;

  public GameLobbyRestServer(int portNumber) {
    gson = new Gson();
    javalin = Javalin.create();
    configureRoutes();
    javalin.start(portNumber);
  }

  private void configureRoutes() {

    // Create Remote Game (i.e. create a FutureGame resource)
    javalin.post("/lobby", ctx ->
    {
      debugOutput(ctx);

      // Demarshall body
      String payload = ctx.body();
      JsonNode asNode = new JsonNode(payload);
      String playerName = asNode.getObject().getString("playerOne");
      Integer level = asNode.getObject().getInt("level");

      // Call 'domain' code to create the future game
      int futureGameId = createFutureGameAndInsertIntoDatabase(playerName, level);
      FutureGameResource fgame = database.get(futureGameId);

      // And construct the response of the POST
      ctx.status(HttpServletResponse.SC_CREATED);
      String location = ctx.host() + "/lobby/" + futureGameId;
      ctx.header("Location", location );

      debugOutput("-< Location: " + location);
      debugOutput("-< Reply: " + gson.toJson(fgame));

      ctx.json(gson.toJson(fgame));
    });

    // Get FutureGame resource
    javalin.get( "/lobby/{futureGameId}", ctx -> {
      debugOutput(ctx);

      String idAsString = ctx.pathParam("futureGameId");
      // TODO: Handle non-integer provided as path
      Integer id = Integer.parseInt(idAsString);

      FutureGameResource fgame = getFutureGameFromDatabase(id);

      ctx.status(HttpServletResponse.SC_OK);

      debugOutput("-< Reply: " + gson.toJson(fgame));

      ctx.json(gson.toJson(fgame));
    });

    // Partial update the FutureGame resource, thereby transition into a valid game
    javalin.post( "/lobby/{futureGameId}", ctx -> {
      debugOutput(ctx);

      String idAsString = ctx.pathParam("futureGameId");
      // TODO: Handle non-integer provided as path
      Integer id = Integer.parseInt(idAsString);

      FutureGameResource fgame = database.get(id);

      // Demarshall body
      String payload = ctx.body();
      JsonNode asNode = new JsonNode(payload);

      String playerTwo = asNode.getObject().getString("playerTwo");

      // Update resource
      fgame.setPlayerTwo(playerTwo);
      fgame.setAvailable(true);

      // Create game instance
      int gameId = createGameResourceAndInsertIntoDatabase(fgame);

      fgame.setNext("/lobby/game/" + gameId);
      updateFutureGameInDatabase(id, fgame);

      debugOutput("-< Reply: " + gson.toJson(fgame));

      ctx.json(gson.toJson(fgame));
    });

    // Get Game resource
    javalin.get( "/lobby/game/{gameId}", ctx -> {
      debugOutput(ctx);

      String idAsString = ctx.pathParam("gameId");
      // TODO: Handle non-integer provided as path
      int id = Integer.parseInt(idAsString);

      ctx.status(HttpServletResponse.SC_OK);

      GameResource game = getGameFromDatabase(id);

      debugOutput("-< Reply: " + gson.toJson(game));

      ctx.json(gson.toJson(game));
    });

    // GET on a move resource
    javalin.get( "/lobby/game/{gameId}/move/{moveId}", ctx -> {
      debugOutput(ctx);
      String gameIdAsString = ctx.pathParam("gameId");
      // TODO: Handle non-integer provided as gameId
      Integer gameId = Integer.parseInt(gameIdAsString);

      String moveIdAsString = ctx.pathParam("moveId");
      // TODO: Handle non-integer provided as moveId
      Integer moveId = Integer.parseInt(moveIdAsString);

      // TODO: handle out of bounds and null values
      MoveResource move = getMoveResourceFor( gameId, moveId);

      debugOutput("-< Reply: " + gson.toJson(move));

      ctx.status(HttpServletResponse.SC_OK);

      ctx.json(gson.toJson(move));
    });

    // Update the move resource, i.e. making a transition in the game's state
    javalin.put( "/lobby/game/{gameId}/move/{moveId}", ctx -> {
      debugOutput(ctx);

      String gameIdAsString = ctx.pathParam("gameId");
      // TODO: Handle non-integer provided as path
      int gameId = Integer.parseInt(gameIdAsString);

      // Demarshall body
      String payload = ctx.body();
      MoveResource move = gson.fromJson(payload, MoveResource.class);

      // Update game resource with the new move
      GameResource game = getGameFromDatabase(gameId);
      makeTheMove(game, move);

      // TODO: Implement logic to handle an invalid move
      // Simply return the same object as PUT
      debugOutput("-< Reply: " + gson.toJson(move));
      ctx.json(gson.toJson(move));
    });

  }

  // === Domain handling of FutureGame resources, all FAKE-OBJECT code
  private int futureGameId = 42;
  private final Map<Integer, FutureGameResource> database
          = new HashMap<>();

  private int createFutureGameAndInsertIntoDatabase(String playerName, Integer level) {
    FutureGameResource f = new FutureGameResource(playerName, level);
    database.put(futureGameId, f);
    futureGameId++;

    return futureGameId - 1;
  }
  private FutureGameResource getFutureGameFromDatabase(int id) {
    return database.get(id);
  }
  private void updateFutureGameInDatabase(int id, FutureGameResource fgame) {
    database.put(id, fgame);
  }

  // === Domain handling of Game and Move resources
  private GameResource theOneGameOurServerHandles;

  private int createGameResourceAndInsertIntoDatabase(FutureGameResource fgame) {
    // Fake it code - we only handle a single game instance with id = 77;
    int theGameId = 77;

    // Create the move resource storage
    createMoveResourceListForGame(theGameId);

    // Create the game resource
    theOneGameOurServerHandles =
            new GameResource(fgame.getPlayerOne(), fgame.getPlayerTwo(),
                    fgame.getLevel(), theGameId);

    return theGameId;
  }

  private GameResource getGameFromDatabase(int id) {
    // Fake-it - only one game instance handled
    return theOneGameOurServerHandles;
  }

  // MOVE Domain code
  private ArrayList<MoveResource> moveResourceList;
  private void createMoveResourceListForGame(int theGameId) {
    moveResourceList = new ArrayList<>();
    // Create first move
    MoveResource move = new MoveResource("null","null", "null");
    moveResourceList.add(move);
  }

  private MoveResource makeTheMove(GameResource game, MoveResource move) {
    moveResourceList.set(moveResourceList.size()-1, move);
    game.makeAMove(move);
    MoveResource nextMove = new MoveResource("null","null", "null");
    moveResourceList.add(nextMove);
    // Return the original made move, as defined by PUT
    return move;
  }

  private MoveResource getMoveResourceFor(Integer gameId, Integer moveId) {
    return moveResourceList.get(moveId);
  }


  public void stop() {
    javalin.stop();
  }

  private void debugOutput(Context ctx) {
    String body = ctx.body();
    debugOutput("-> " +  ctx.method() + " " + ctx.path()
            + ": " + body);
  }
  private void debugOutput(String s) {
    // Enable the printing below to review the request/reply
    // of the GameLobby REST server.
    System.out.println(s);
  }
}

/*
 * Copyright (C) 2018-2025. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package telemed.rest;

import java.util.List;

import com.google.gson.*;

import frds.broker.Invoker;
import frds.broker.ServerRequestHandler;
import frds.broker.ipc.http.MimeMediaType;
import io.javalin.Javalin;
import jakarta.servlet.http.HttpServletResponse;
import telemed.domain.TeleMed;
import telemed.domain.TeleObservation;
import telemed.domain.TimeInterval;
import telemed.ipc.http.Constants;
import telemed.storage.XDSBackend;

/** A REST based implementation of the ServerRequestHandler and Invoker
 * roles using Javalin.
 * <p>
 * As a REST servlet handles IPC as well as the location aspects of the
 * Invoker, it makes the code much shorter and easier to follow to merge
 * the two roles.
 * <p>
 * NOTE: It also implements the Invoker role, but not the method
 * in the Invoker interface from the Broker (handleRequest()). If
 * called, an UnsupportedOperationException is thrown.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
public class RESTServerRequestHandlerInvoker
    implements ServerRequestHandler, Invoker {

  private final Javalin javalin;
  private TeleMed teleMed;
  private int port;
  private Gson gson;

  public RESTServerRequestHandlerInvoker(int portNumber,
      TeleMed tsServant, XDSBackend xds) {
    this.teleMed = tsServant;
    this.port = portNumber;

    gson = new Gson();
    javalin = Javalin.create();
  }

  @Override
  public void setPortAndInvoker(int port, Invoker invoker) {
    setPortAndInvoker(port, invoker, false);
  }

  @Override
  public void setPortAndInvoker(int port, Invoker invoker, boolean useTLS) {
    this.port = port;
    // invoker not relevant.
  }


  @Override
  public void start() {
    // POST = processAndStore
    String storeRoute = "/" + Constants.BLOODPRESSURE_PATH;
    javalin.post(storeRoute, ctx -> {
      String body = ctx.body();

      // Demarshal parameters into a JsonArray
      TeleObservation teleObs = gson.fromJson(body, TeleObservation.class);
      String id = teleMed.processAndStore(teleObs);

      // Normally: 201 Created
      ctx.status(HttpServletResponse.SC_CREATED);
      ctx.contentType(MimeMediaType.APPLICATION_JSON);

      // Location = URL of created resource
      ctx.header("Location",
          ctx.host() + "/" + Constants.BLOODPRESSURE_PATH + id);

      // Return the tele observation as confirmation
      ctx.json(gson.toJson(teleObs));
    });

    // GET = getObservation
    String getRoute = "/" + Constants.BLOODPRESSURE_PATH + "{id}";

    javalin.get(getRoute, ctx -> {
      String uniqueId = ctx.pathParam("id");

      TeleObservation teleObs = teleMed.getObservation(uniqueId);

      String returnValue = null;
      int lastStatusCode;
      
      if (teleObs == null) {
        lastStatusCode = HttpServletResponse.SC_NOT_FOUND;
        returnValue = "{}"; // a null JSON object
      } else {
        lastStatusCode = HttpServletResponse.SC_OK;
        returnValue = gson.toJson(teleObs);
      }

      ctx.status(lastStatusCode);
      ctx.contentType(MimeMediaType.APPLICATION_JSON);

      ctx.json(returnValue);
    });

    // PUT = correct
    String correctRoute = "/" + Constants.BLOODPRESSURE_PATH + "{id}";
    javalin.put(correctRoute, ctx -> {
      String body = ctx.body();
      String uniqueId = ctx.pathParam("id");

      TeleObservation teleObs = gson.fromJson(body, TeleObservation.class);

      boolean isValid = teleMed.correct(uniqueId, teleObs);

      // According to RFC 7231, PUT returns 200 OK
      int lastStatusCode = HttpServletResponse.SC_OK;
      if (!isValid) {
        lastStatusCode = HttpServletResponse.SC_NOT_FOUND;
      }

      ctx.status(lastStatusCode);
      ctx.contentType(MimeMediaType.APPLICATION_JSON);

      ctx.result("{}");
    });

    // DELETE = delete
    String deleteRoute = "/" + Constants.BLOODPRESSURE_PATH + "{id}";
    javalin.delete(deleteRoute, ctx -> {
      String uniqueId = ctx.pathParam("id");

      boolean isValid = teleMed.delete(uniqueId);

      int lastStatusCode = HttpServletResponse.SC_NO_CONTENT;
      if (!isValid) {
        lastStatusCode = HttpServletResponse.SC_NOT_FOUND;
      }

      ctx.status(lastStatusCode);
      ctx.contentType(MimeMediaType.APPLICATION_JSON);

      ctx.result("{}");
    });

    // GET HTML = getObservationFor
    String getHtmlRoute = "/"+ Constants.BLOODPRESSURE_PATH + "/for/{patientId}";

    javalin.get(getHtmlRoute, ctx -> {
      String patientId = ctx.pathParam("patientId");
      System.out.println("--> FOR "+patientId);

      List<TeleObservation> theList = teleMed.getObservationsFor(patientId,
          TimeInterval.LAST_DAY);

      ctx.status(HttpServletResponse.SC_OK);
      ctx.contentType(MimeMediaType.HTML);

      String returnValue = "<h1>Observations for " + patientId + "</h1>\n";
      returnValue += "<ol>";
      for (TeleObservation to : theList) {
        returnValue += "<li>" + to.toString() + "</li>";
      }
      returnValue += "</ol>";

      ctx.html(returnValue);
    });

    javalin.start(port);
  }

  @Override
  public void stop() {
    javalin.stop();
  }

  @Override
  public String handleRequest(String request) {
    throw new UnsupportedOperationException("The REST based invoker does NOT use the handleRequest method!");
  }
}

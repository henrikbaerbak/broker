FRDS.Broker Library Version History
====

  * Version 1.1: Initial Release.
  
  * Version 1.2: The IPCException may now contain HTTP status code.
  
  * Version 1.3: Empty constructors for the request handlers introduced.

  * Version 1.4: Marshalling format version can now be set.
  
  * Version 1.5: Changed the logging output for
                 URITunnelServerRequestHandler, modified the toString
                 of request and reply objects.
                 
  * Version 1.6: Added response time calculations in
                 URITunnelServerRequestHandler, and output it in logs.

  * Version 1.7: Changed log output format to key-value in 
                 URITunnelServerRequestHandler

  * Version 2.0: Major overhaul of internal broker role interfaces,
                 to clean up the issue that marshalling was partially
                 done in the CRH and SRH. From this version on, marshalling
                 and demarshalling is only made in the Requestor and
                 Invoker layer of the pattern, while CRH and SRH handles
                 Strings only. 
                 THIS VERSION BREAKS THE CRH and INVOKER API. Therefore
                 client code must be rewritten to utilize this version.

  * Version 2.0.1: Stepping stone release, sole purpose is migrating
                from JCenter() to mavenCentral() repositories.
  
  * Version 2.1: Wrong mavenCentral() upload, is empty :(
  
  * Version 3.0: Introduced TLS/HTTPS handling in the URI Tunnel
                 request handlers, for secure network communication,
                 as well writtena a short introduction. Updates to
                 RequestHandlers's 'set' methods, while default
                 methods introduced to allow backward compatibility.
  * Version 3.0.1: Last Java 8 version
  * Version 3.0.2: Java 11 version; updated dependencies.
  * Version 3.0.3: Updated dependency to non-vulnerable jetty-server
  * Version 4.0:   Updated to UniRest 4.3 causing changed imports;
                   lifted all tests to JUnit 5; updated Jetty deps
                   to version 9.4.54
  * Version 4.0.1: Lifted to Log4J 2 as well as updated Jetty deps.

  * Version 5.0:   Refactored to use *Javalin* as Jetty facade as
                   Spark-java seemingly is not maintained anymore.
                   Thus, any Spark-java used in client code will not
                   work anymore.
                   SSL/HTTPS support temporarily disabled.

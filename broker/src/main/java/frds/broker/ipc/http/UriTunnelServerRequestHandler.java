/*
 * Copyright (C) 2018-2025. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package frds.broker.ipc.http;

import com.google.gson.Gson;

import frds.broker.Invoker;
import frds.broker.ServerRequestHandler;

import frds.broker.ipc.SSLPropertyConstants;

import io.javalin.Javalin;
import io.javalin.http.ContentType;

import jakarta.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** ServerRequestHandler implementation using HTTP and URI Tunneling.
 * <p>
 * Implementation based on the Javalin framework.
 *
 */

public class UriTunnelServerRequestHandler
        implements ServerRequestHandler {

  public static final String DEFAULT_URI_TUNNEL_PATH = "tunnel";

  protected final Gson gson;
  private boolean useTLS;
  protected Invoker invoker;
  protected int port;
  protected String lastVerb;
  protected String tunnelRoute;
  protected final Logger logger;

  // provide protected access to the Javalin instance, so
  // subclasses may add own routes.
  private final Javalin javalinInstance;

  /** Create a URI Tunnel based server request handler,
   * defaulting to path '/tunnel'. Remember to set port
   * and Invoker before starting the server process.
   */
  public UriTunnelServerRequestHandler() {
    gson = new Gson();
    logger = LoggerFactory.getLogger(UriTunnelServerRequestHandler.class);
    tunnelRoute = DEFAULT_URI_TUNNEL_PATH;
    useTLS = false;

    javalinInstance = Javalin.create();
  }

  @Override
  public void setPortAndInvoker(int port, Invoker invoker) {
    setPortAndInvoker(port, invoker, false);
  }

  @Override
  public void setPortAndInvoker(int port, Invoker invoker, boolean useTLS) {
    logger.info("method=setPortAndInvoker, port={}, useTLS={}", port, useTLS);
    this.port = port; this.invoker = invoker; this.useTLS = useTLS;
  }

  /**
   * Construct a full URI Tunnel SRH with given invoker, port, and
   * tunnel route. Optionally allow TLS/HTTPS communication (which
   * requires keystore to be defined.)
   * @param invoker the Broker invoker to forward incoming messages to
   * @param port the port for listening to incoming messages
   * @param useTLS if true, switch to HTTPS communication (see additional
   *               README for some info on how this works.)
   * @param tunnelRoute the route/path to listen to
   */
  public UriTunnelServerRequestHandler(Invoker invoker, int port, boolean useTLS, String tunnelRoute) {
    this();
    setPortAndInvoker(port, invoker);
    this.tunnelRoute = tunnelRoute;
    this.useTLS = useTLS;
    logger.info("method=constructur, port={}, uri_tunnel_path={}, tls={}", port, this.tunnelRoute, useTLS);
  }

  /**
   * Construct a full URI Tunnel SRH with given invoker, port, and
   * tunnel route. O
   * @param invoker the Broker invoker to forward incoming messages to
   * @param port the port for listening to incoming messages
   * @param tunnelRoute the route/path to listen to
   */
  public UriTunnelServerRequestHandler(Invoker invoker,
                                       int port, String tunnelRoute) {
    this(invoker, port, false, tunnelRoute);
  }

  @Override
  public void start() {
    // if required to use TLS then get the system properties and secure the connection
    String keystoreFilename = "undefined";
    if (useTLS) {
      // Get the System Properties of the key store; remember to set them correctly
      // using a 'systemProperty' if running from Gradle.
      keystoreFilename = System.getProperty(SSLPropertyConstants.JAVAX_NET_SSL_KEYSTORE);
      String keystorePassword = System.getProperty(SSLPropertyConstants.JAVAX_NET_SSL_KEYSTORE_PASSWORD);
      logger.warn("TLS disabled at the time being.");
      // TODO: Review A quick intro to SSL in Java / https://javalin.io/tutorials/javalin-ssl-tutorial
      // Spark-java way: secure(keystoreFilename, keystorePassword, null, null);
    }

    logger.info("method=start, port={}, tls={}, keystore='{}'",
            port, useTLS, keystoreFilename);

    // POST is for all incoming requests, and they are plain text
    // format as we cannot know the marshalling format in advance
    javalinInstance.post(tunnelRoute, ctx -> {
      long startTime = System.currentTimeMillis();
      String marshalledRequest = ctx.body();
      
      logger.info("method=POST, context=request, request={}", marshalledRequest);

      // The incoming marshalledRequest is the marshalled request to the invoker
      String reply = invoker.handleRequest(marshalledRequest);

      // We process any byte array, the requestor/invoker pair defines the marshalling
      ctx.contentType(ContentType.TEXT_PLAIN);

      // Store the last verb and status code to allow spying during test
      lastVerb = ctx.method().name();

      // The reply is opaque - so we have no real chance of setting a proper
      // status code.
      ctx.status(HttpServletResponse.SC_OK);

      // response time in milliseconds for invoker upload is calculated
      long responseTime = System.currentTimeMillis() - startTime;
      logger.info("method=handleRequest, context=reply, reply={}, responseTime_ms={}",
              reply, responseTime);

      ctx.json(reply);
      //return reply;
    });

    // Start Javalin on the defined port
    javalinInstance.start(port);
  }

  @Override
  public void stop() {
    javalinInstance.stop();
  }

  /** Users may need to add own routes, so allow access to the
   * underlying Javalin instance. For an example, see the
   * TeleMed URITunnelServerRequestHandler.
   *
   * @return the singleton javalin instance encapsulating
   * the Jetty web server.
   */
  public Javalin getJavalinInstance() {
    return javalinInstance;
  }

  @Override
  public String toString() {
    return getClass().getCanonicalName() + ", port " + port +
        ", root path: '" + tunnelRoute + "'";
  }

  public String lastHTTPVerb() {
    return lastVerb;
  }

}

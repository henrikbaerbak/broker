PasteBin - a Case Study in the FRDS book
========================================

This is *PasteBin*, a simple case study in using HTTP verbs to handle
information in a web server.

Context
---

Find an explanation to the code base in the [Flexible, Reliable,
Distributed Software](https://leanpub.com/frds) book by *Henrik Bærbak
Christensen*, Aarhus University.

How to run?
---

Start the web server using Gradle

    gradle pastebin
    
This will start the pastebin server
on [localhost:4567/bin](localhost:4567/bin).
    
    
Next you can use PostMan, Curl, or Httpie to send GET, and POST messages to the
web server. The sequence below is shown using Httpie.

### Create a clip (POST)


    csdev@debian25:~/proj/broker-internal/pastebin$ http POST localhost:4567/bin contents=horse
    HTTP/1.1 201 Created
    Content-Type: application/json
    Date: Thu, 23 Jan 2025 08:45:05 GMT
    Location: localhost:4567/bin/100
    Server: Jetty(9.4.54.v20240208)
    Transfer-Encoding: chunked

    {
        "contents": "horse"
    }
### Retrieve clip (GET)

    csdev@debian25:~/proj/broker-internal/pastebin$ http localhost:4567/bin/100
    HTTP/1.1 404 Not Found
    Content-Length: 20
    Content-Type: application/json
    Date: Thu, 23 Jan 2025 09:04:12 GMT

    {
        "contents": "horse"
    }

    
### Retrieve unknown clip (GET)

    csdev@debian25:~/proj/broker-internal/pastebin$ http localhost:4567/bin/102
    HTTP/1.1 404 Not Found
    Content-Length: 4
    Content-Type: application/json
    Date: Thu, 23 Jan 2025 09:05:03 GMT

    null


Note on Design
---

The PasteBin design is purely to demonstrate POST/GET in a RESTish
architectural style, and coded to minimize the code base size, thus
the domain code (storing and retrieving clips from a clipboard) is
embedded in the REST server role, `Server`, by hardcoding a HashMap of
`Bin` instances. A better design would of course separate the domain
code into suitable roles and implementations of these roles.

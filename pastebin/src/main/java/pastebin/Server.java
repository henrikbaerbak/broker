package pastebin;

import java.util.*;

import com.google.gson.Gson;
import io.javalin.Javalin;
import jakarta.servlet.http.HttpServletResponse;

/** Controller class that defines the CRUD REST verbs.
 * 
 * Reference: Jim Webber, Chapter 3.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
public class Server {

  private int id = 100;
  private Gson gson = new Gson();
  private Map<String,Bin> db = new HashMap<String, Bin>();

  public static void main(String[] args) {
    new Server();
  }
  
  public Server() {
    Javalin app = Javalin.create();
    /**
     * POST /bin. Create a new bin, if success, receive a Location header
     * specifying the bin's resource identifier.
     * 
     * Parameter: req.body must be JSON such as {"contents":
     * "Suzy's telephone no is 1234"}
     */
    app.post("/bin", ctx -> {
        // Convert from JSON into object format
        Bin q = gson.fromJson(ctx.body(), Bin.class);
        
        // Create a new resource ID
        String idAsString = ""+id++;
        
        // Store bin in the database
        db.put(idAsString, q);
        
        // 201 Created
        ctx.status(HttpServletResponse.SC_CREATED);
        // Set return type
        ctx.contentType("application/json");
        // Location = URL of created resource
        ctx.header("Location", ctx.host()+"/bin/"+idAsString);

        ctx.json(gson.toJson(q));
        
      });


    /**
     * GET /bin/<id>. Get the bin with the given id
     */
    app.get("/bin/{id}", ctx -> {
      // Extract the bin id from the request
      String id = ctx.pathParam("id");

      // Set return type
      ctx.contentType("application/json");

      // Lookup, and return if found
      Bin bin = db.get(id);
      if (bin != null) {
        // Status = OK
        ctx.status(HttpServletResponse.SC_OK);
        // Return contents
        ctx.json(gson.toJson(bin));

      } else {
        // Otherwise, return error
        ctx.status(HttpServletResponse.SC_NOT_FOUND);
        // return null
        ctx.json(gson.toJson(bin));
      }
    });

    app.start(4567);
  }
}
